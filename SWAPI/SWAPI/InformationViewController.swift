//
//  InformationViewController.swift
//  SWAPI
//
//  Created by Fredrik Holm on 2017-11-24.
//  Copyright © 2017 Fredrik Holm. All rights reserved.
//

import UIKit

class InformationViewController: UIViewController {

    var sentStarship: Starship?

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var classLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var crewLabel: UILabel!
    @IBOutlet weak var manufacturerLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = sentStarship?.name
        classLabel.text = sentStarship?.starship_class
        modelLabel.text = sentStarship?.model
        crewLabel.text = "crew: \(sentStarship?.crew ?? "0")"
        manufacturerLabel.text = sentStarship?.manufacturer
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
