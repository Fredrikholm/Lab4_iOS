//
//  ViewController.swift
//  SWAPI
//
//  Created by Fredrik Holm on 2017-11-24.
//  Copyright © 2017 Fredrik Holm. All rights reserved.
//

import UIKit

class StarshipCell: UITableViewCell {
    @IBOutlet weak var starshipName: UILabel!
}
class StarshipRaw: Codable {
    var count: Int?
    var results: [Starship]?
}

struct Starship: Codable {
    let name: String?
    let model: String?
    let manufacturer: String?
    let starship_class: String?
    let crew: String?
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var tableView: UITableView!
    var starships = StarshipRaw()
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        getStarships()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    
    func getStarships() {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "swapi.co"
        urlComponents.path = "/api/starships"
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "GET"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        print("Going to get starships!")
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                // print("Did we get starships? data =", data, "resp ? ", response, " error =", error)
                guard error == nil else {
                    return
                }
                
                guard let json = data else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Data was not retrieved from request"]) as Error
                    print(error)
                    return
                }
                
                let decoder = JSONDecoder()
                
                print("Will start decoding...")
                do {
                    self.starships = try decoder.decode(StarshipRaw.self, from: json)
                    print("Success ? ", self.starships.count as Any)
                    
                    var i = 0
                    self.tableView.beginUpdates()
                    for starship in self.starships.results! {
                        print(starship.name as Any)
                        self.tableView.insertRows(at: [IndexPath(row: i, section: 0)], with: .automatic)
                        i += 1
                    }
                    self.tableView.endUpdates()
                    
                } catch {
                    print("There were an error")
                }
            }
        }.resume()
    }
    
    // FUNCTIONS FOR SEGUE
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("\n\n\n")
        print("starting segue...")
        let vc = segue.destination as! InformationViewController
        
        if let starshipToBeSent = (sender as? StarshipCell)?.starshipName.text {
            for starship in starships.results! {
                if starship.name == starshipToBeSent {
                    vc.sentStarship = starship
                    break
                }
            }
        }
    }
    
    
    // FUNCTIONS FOR TABLEVIEW
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return starships.results?.count ?? 0
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "starshipCell") as! StarshipCell
        cell.starshipName.text = self.starships.results![indexPath.row].name
        return cell
    }
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "SW Starships"
    }
}






